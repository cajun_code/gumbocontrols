//
//  TableViewTextFieldRevolver.m
//  TripFiles
//
//  Created by Allan Davis on 6/4/14.
//  Copyright (c) 2014 TripFiles. All rights reserved.
//

#import "CCTextFieldRevolver.h"

@interface CCTextFieldRevolver()

@property NSInteger selectedIndex;

@property (strong, nonatomic) NSMutableArray *textFields;

@end

@implementation CCTextFieldRevolver

-(instancetype)init{
	self = [super init];
	if (self) {
		_showDoneButton = YES;
	}
	return self;
}

-(void) registerTextField:(UITextField *)textField{
    if (!self.textFields) {
        self.textFields = @[].mutableCopy;
        
    }
    textField.delegate = self;
    [self.textFields addObject:textField];
}

#pragma - mark UITextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    self.selectedIndex = [self.textFields indexOfObject: textField];
    UIToolbar *doneToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    doneToolbar.barStyle = UIBarButtonItemStylePlain;
    doneToolbar.tintColor = [UIColor blackColor];
    NSMutableArray* items = [NSMutableArray arrayWithObjects:
                         [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Arrows-Back-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(selectPreviousTextField)],
                         [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Arrows-Forward-icon"] style:UIBarButtonItemStylePlain target:self action:@selector(selectNextTextField)],
                         [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],                         nil];
	if (_showDoneButton) {
		[items addObject:[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)]];
	}
	doneToolbar.items = items;
    //UITextField * nextTextField = (UITextField *)[self.textFields objectAtIndex: self.selectedIndex +1];
    if (self.selectedIndex <= 0) [[doneToolbar.items objectAtIndex:0] setEnabled:NO];
    if ((self.selectedIndex + 1) == [self.textFields count]) [[doneToolbar.items objectAtIndex:1] setEnabled:NO];

    if ( _showDoneToolbar ) {
        textField.inputAccessoryView = doneToolbar;
    }
    
    
    if (self.delegate) {
        [self.delegate hasEnteredTextField:textField];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    UITextField * nextTextField = nil;
    if (self.selectedIndex < [self.textFields count] -1)
        nextTextField = (UITextField *)[self.textFields objectAtIndex: self.selectedIndex +1];
    if (nextTextField && textField.returnKeyType == UIReturnKeyNext) {
        [nextTextField becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
        self.submit(textField);
    }
    return YES;
}

- (void)selectNextTextField{
    UITextField * nextTextField = (UITextField *)[self.textFields objectAtIndex: self.selectedIndex +1];
    [nextTextField becomeFirstResponder];
}

- (void)selectPreviousTextField{
    UITextField * previousTextField =  (UITextField *)[self.textFields objectAtIndex: self.selectedIndex -1];
    [previousTextField becomeFirstResponder];
}

- (void)dismissKeyboard {
    [self.textFields[self.selectedIndex] resignFirstResponder];
}

@end
