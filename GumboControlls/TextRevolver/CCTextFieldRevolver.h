//
//  TableViewTextFieldRevolver.h
//  TripFiles
//
//  Created by Allan Davis on 6/4/14.
//  Copyright (c) 2014 TripFiles. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *
 *
 *  @param textField <#textField description#>
 */
typedef void(^SubmitBlock)(UITextField* textField);

/**
 *  
 */
@protocol CCTextFieldRevolverDelegate <NSObject>


-(void) hasEnteredTextField:(UITextField*)textfield;

@end




@interface CCTextFieldRevolver : NSObject<UITextFieldDelegate>

-(void) registerTextField:(UITextField *)textField;

@property (strong, nonatomic) SubmitBlock submit;

@property (strong, nonatomic) id<CCTextFieldRevolverDelegate> delegate;

@property (nonatomic) BOOL showDoneButton;
@property (nonatomic) BOOL showDoneToolbar;

@end
