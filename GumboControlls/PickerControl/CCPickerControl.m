//
//  CCPickerControl.m
//  GumboControlls
//
//  Created by Allan Davis on 8/10/14.
//  Copyright (c) 2014 Cajun Code Software. All rights reserved.
//

#import "CCPickerControl.h"

@interface CCPickerControl ()<UIPickerViewDataSource, UIPickerViewDelegate>{
    SelectionHandler _handler;
}

@property (strong, nonatomic) UIPickerView* pickerView;
@property (strong, nonatomic) UIActionSheet *actionSheet;
@property NSInteger selectedIndex;

@end

@implementation CCPickerControl

-(SelectionHandler) selectionHandler{
    return _handler;
}

-(void) setSelectionHandler:(SelectionHandler)selectionHandler{
    _handler = selectionHandler;
}

-(void) showPicker{
    self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    
    [self.actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    self.pickerView = [[UIPickerView alloc] initWithFrame:pickerFrame];
    [self.pickerView selectRow:self.selectedIndex inComponent:0 animated:YES];
    
    self.pickerView.showsSelectionIndicator = YES;
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    
    [self.actionSheet addSubview:self.pickerView];
    //[pickerView release];
    
    
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Close"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    //closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [self.actionSheet addSubview:closeButton];
    //[closeButton release];
    
    [self.actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    
    [self.actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) dismissActionSheet:(id) sender{
    
    if (self.selectionHandler) {
        self.selectionHandler(self.values[self.selectedIndex]);
    }
    
    [self.actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}


-(instancetype) initWithValues:(NSArray*) values andValueSelected:(NSString*) selected withHandler:(SelectionHandler) handler{
    self = [super init];
    if(self){
        // init objects here
        _values = values;
        _selectedIndex = [values indexOfObject:selected];
        _handler = handler;
    }
    return self;
}

# pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [self.values count];
}

# pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    self.selectedIndex = row;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return self.values[row];
}

@end
