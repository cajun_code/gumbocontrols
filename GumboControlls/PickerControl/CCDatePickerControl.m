//
//  CCDatePickerControl.m
//  GumboControlls
//
//  Created by Allan Davis on 8/10/14.
//  Copyright (c) 2014 Cajun Code Software. All rights reserved.
//

#import "CCDatePickerControl.h"

@interface CCDatePickerControl (){
    SelectedDateHandler _handler;
}

@property (strong, nonatomic) UIDatePicker* pickerView;
@property (strong, nonatomic) UIActionSheet *actionSheet;

@end


@implementation CCDatePickerControl

-(SelectedDateHandler) selectionHandler{
    return _handler;
}

-(void) setSelectionHandler:(SelectedDateHandler)selectionHandler{
    _handler = selectionHandler;
}

-(void) showDatePicker{
    self.actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    
    [self.actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    CGRect pickerFrame = CGRectMake(0, 40, 0, 0);
    
    self.pickerView = [[UIDatePicker alloc] initWithFrame:pickerFrame];
    self.pickerView.datePickerMode = UIDatePickerModeDate;
    self.pickerView.date = self.startDate;
    //pickerView.showsSelectionIndicator = YES;
    //pickerView.dataSource = self;
    //pickerView.delegate = self;
    
    [self.actionSheet addSubview:self.pickerView];
    //[pickerView release];
    
    UISegmentedControl *closeButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Close"]];
    closeButton.momentary = YES;
    closeButton.frame = CGRectMake(260, 7.0f, 50.0f, 30.0f);
    //closeButton.segmentedControlStyle = UISegmentedControlStyleBar;
    closeButton.tintColor = [UIColor blackColor];
    [closeButton addTarget:self action:@selector(dismissActionSheet:) forControlEvents:UIControlEventValueChanged];
    [self.actionSheet addSubview:closeButton];
    //[closeButton release];
    
    [self.actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
    
    [self.actionSheet setBounds:CGRectMake(0, 0, 320, 485)];
}

-(void) dismissActionSheet:(id) sender{
    if (self.selectionHandler) {
        self.selectionHandler(self.pickerView.date);
    }
    
    [self.actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}


-(instancetype) initWithStartDate:(NSDate *)startDate andSelectionHandler:(SelectedDateHandler) handler{
    self = [super init];
    if(self){
        _handler = handler;
        _startDate = startDate;
    }
    return self;
}


@end
