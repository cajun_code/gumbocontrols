//
//  CCPickerControl.h
//  GumboControlls
//
//  Created by Allan Davis on 8/10/14.
//  Copyright (c) 2014 Cajun Code Software. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SelectionHandler)(NSString *data);

@interface CCPickerControl : NSObject

@property SelectionHandler selectionHandler;

@property (strong, nonatomic) NSArray *values;

-(void) showPicker;

-(instancetype) initWithValues:(NSArray*) values andValueSelected:(NSString*) selected withHandler:(SelectionHandler) handler;

@end
