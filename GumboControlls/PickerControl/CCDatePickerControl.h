//
//  CCDatePickerControl.h
//  GumboControlls
//
//  Created by Allan Davis on 8/10/14.
//  Copyright (c) 2014 Cajun Code Software. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SelectedDateHandler)(NSDate *date);

@interface CCDatePickerControl : NSObject

-(void) showDatePicker;

-(instancetype) initWithStartDate:(NSDate *)startDate andSelectionHandler:(SelectedDateHandler) handler;

@property SelectedDateHandler selectionHandler;

@property (strong, nonatomic) NSDate *startDate;  

@end
