//
//  main.m
//  GumboControlls
//
//  Created by Allan Davis on 8/10/14.
//  Copyright (c) 2014 Cajun Code Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CCAppDelegate class]));
    }
}
