Pod::Spec.new do |s|
  s.name         = "GumboControls"
  s.version      = "0.0.2"
  s.summary      = "My Collection of iOS Controls"

  s.description  = <<-DESC
Collection of iOS Controls for Picker control and date picker 
                   DESC

  s.homepage     = "https://bitbucket.org/cajun_code/gumbocontrols"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author             = { "Allan Davis" => "cajun.code@gmail.com" }
  s.social_media_url   = "http://twitter.com/cajun_code"

  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://bitbucket.org/cajun_code/gumbocontrols.git", :tag => "v0.0.2" }
  s.source_files  = "GumboControlls/TextRevolver/*.{h,m}", "GumboControlls/PickerControl/*.{h,m}"
  s.requires_arc = true
  s.framework  = 'UIKit'
end
