//
//  PopUpDatePickerViewController.h
//  FalconUniversal
//
//  Created by Allan Davis on 3/27/15.
//  Copyright (c) 2015 DaVita. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ChangeHandler)(NSDate *newDate);

@interface PopUpDatePickerViewController : UIViewController

@property (strong, nonatomic) ChangeHandler change;
@property (strong, nonatomic) NSDate *initialDate;
@property (strong, nonatomic) NSDate *minDate;
@property (strong, nonatomic) NSDate *maxDate;


-(instancetype) initWithIntialDate:(NSDate *) initalDate andChangeHandler:(ChangeHandler) handler;

-(void) showPopUpOnViewController:(UIViewController *) presentingVC forElement:(UIView *)element;


@end
