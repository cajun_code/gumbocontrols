//
//  PopUpDatePickerViewController.m
//  FalconUniversal
//
//  Created by Allan Davis on 3/27/15.
//  Copyright (c) 2015 DaVita. All rights reserved.
//

#import "PopUpDatePickerViewController.h"


@interface PopUpDatePickerViewController ()
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) UIPopoverController *popup;
@end

@implementation PopUpDatePickerViewController

-(instancetype) initWithIntialDate:(NSDate *) initalDate andChangeHandler:(ChangeHandler) handler{
    self = [self initWithNibName:@"PopUpDatePickerViewController" bundle:nil];
    if (self) {
        _initialDate = initalDate;
        _change = handler;
    }
    return self;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    // Init date picker
    self.datePicker.date = self.initialDate;
    self.datePicker.maximumDate = self.maxDate;
    self.datePicker.minimumDate = self.minDate;
}

-(void) showPopUpOnViewController:(UIViewController *) presentingVC forElement:(UIView *)element{
    
    if ([self respondsToSelector:@selector(popoverPresentationController)]) {
        // Init popover
        self.modalInPopover = YES;
        self.preferredContentSize = CGSizeMake(320, 200);
        self.modalPresentationStyle = UIModalPresentationPopover;
        self.popoverPresentationController.sourceView = element;
        
        [presentingVC presentViewController:self animated:YES completion:nil];
    }else{
        CGRect elementFrame = [presentingVC.view convertRect:element.frame fromView:element.superview];
        self.popup = [[UIPopoverController alloc] initWithContentViewController:self];
        self.popup.popoverContentSize = CGSizeMake(320.0, 200.0);
        [self.popup presentPopoverFromRect:elementFrame
                                              inView:presentingVC.view
                            permittedArrowDirections:UIPopoverArrowDirectionAny
                                            animated:YES];

    }
    
    

}
- (IBAction)dismissPopUp:(id)sender {
    if (self.popup) {
        [self.popup dismissPopoverAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)datePickerChanged:(UIDatePicker *)sender {
    if (self.change) {
        self.change(sender.date);
    }
}

@end
